(function () {

  'use strict';

  angular
    .module('seed-angular')
    .controller('MainController', MainController);

  function MainController($http, est_api) {
    var vm = this;
    vm.teams = [];
    vm.query = {};
    vm.fullTeamName = {
      POSA: "POSLOVNO SAVJETOVANJE",
      RIF: "RAČUNOVODSTVO I FINANCIJE",
      ASC: "APP START CONTEST",
      ZNAN: "POPULARIZACIJA ZNANOSTI",
      STARTER: "STARTER",
      PREDS: "PREDSJEDNIŠTVO",
      VIZ: "VIZIONAR",
      IT: "INFORMACIJSKE TEHNOLOGIJE",
      PRAD: "PREDAVANJA I RADIONICE",
      MKT: "MARKETING",
      VK: "VIZUALNE KOMUNIKACIJE",
      LJP: "LJUDSKI POTENCIJALI",
      MULTIM: "MULTIMEDIJA",
      PR: "ODNOSI S JAVNOŠĆU (PR)",
      eAMB: "eAMBASADOR",
      IC: "MEĐUNARODNA SURADNJA",
      EBOJ: "ELEKTROBOJ",
      SMP: "SMARTUP",
      DOP: "DRUŠTVENO ODGOVONO POSLOVANJE",
      MCC: "MOOT COURT CROATIA",
      MZG: "MOZGALO",
      CSC: "CASE STUDY COMPETITION"
    };


    function initController() {
      $http.get(est_api.URL + 'teams').success(function (data) {
        vm.teams = data;
      });
    }

    initController();

  }
})();
