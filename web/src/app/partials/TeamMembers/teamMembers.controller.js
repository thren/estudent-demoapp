/**
 * Created by Tomi-PC on 22.12.2015..
 */
(function () {

  'use strict';

  angular
    .module('seed-angular')
    .controller('TeamMembersController', TeamMembersController);

  function TeamMembersController($routeParams, $http, $mdDialog, est_api) {
    var vm = this;
    vm.members = [];
    vm.query = {};

    function initController() {
      $http.get(est_api.URL + 'teams/' + $routeParams.id + '/members').success(function (data) {
        vm.members = data;
      });
    }


    vm.showDialog = showDialog;

    function showDialog() {
      $mdDialog.show({
        controller: DialogController,
        controllerAs: 'dc',
        /*templateUrl: 'dialog.html',*/
        parent: angular.element(document.body),
        template: '<md-dialog>'
        + '<form ng-submit="dc.addNewMember()">'
        + '<md-dialog-content layout="column">'
        + '<h3>Dodaj novog člana</h3>'
        + '<md-input-container>'
        + '<label>Ime</label>'
        + '<input ng-model="firstName" type="text" required>'
        + '</md-input-container>'
        + '<md-input-container>'
        + '<label>Prezime</label>'
        + '<input ng-model="lastName" type="text" required>'
        + '</md-input-container>'
        + '</md-dialog-content>'
        + '<div class="md-actions">'
        + '<md-button ng-click="dc.closeDialog()" class="cancel" type="button">Odustani</md-button>'
        + '<md-button type="submit" class="md-primary add">Dodaj</md-button>'
        + '</div>'
        + '</form>'
        + '</md-dialog>'
      });

      function DialogController($mdDialog, $scope) {
        var self = this;
        self.closeDialog = function () {
          $mdDialog.hide();
        }

        self.addNewMember = function () {
          var member = {};

          member.firstName = $scope.firstName;
          member.lastName = $scope.lastName;
          member.teamId = $routeParams.id;
          console.log(member);
          $http.post("http://est-teams.estudent.hr/api/members", member).then(function () {
            $mdDialog.hide();
          });
        }
      }
    }

    initController();

  }
})();
